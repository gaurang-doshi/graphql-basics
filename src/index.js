import { GraphQLServer } from "graphql-yoga";

// Scaler type = String, Boolean, Int, Float, ID

// Type Defination (Schema)
const typeDefs = `
    type Query {
        hello(name: String): String!
        me: User!
        arraySum(numbers: [Float]!): Float!
    }

    type User {
        id: ID!
        name: String!
        email: String!
        age: Int
    }
`;

// Resolvers
const resolvers = {
  Query: {
    hello(parent, args, contex, info) {
      if (args.name) {
        return `Hello, ${args.name}`;
      } else {
        return "Hello!";
      }
    },
    me() {
      return {
        id: "test122323",
        name: "gdoshi",
        email: "gdoshi@gmail.com",
      };
    },
    arraySum(parent, args, contex, info) {
      if (args.numbers === 0) {
        return 0;
      }
      return args.numbers.reduce((acc, currentVal) => acc + currentVal);
    },
  },
};

const graphServer = new GraphQLServer({
  typeDefs,
  resolvers,
});

graphServer.start(() => {
  console.log("Graph Server is up...");
});
